package main

import (
	"fmt"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	e.GET("/hello", func(c echo.Context) error {
		name := c.QueryParam("name")
		resp := fmt.Sprintf("helloo %s", name)
		return c.HTML(200, resp)
	})

	e.GET("/slots", func(c echo.Context) error {
		resp := map[string]string{
			"ping":    "pong",
			"merhaba": "kartalgozu",
		}
		return c.JSON(200, resp)
	})

	e.Start(":8081")
}
